$(document).ready(function(){
    let filas = 20;
    let cols = 10;
    var score = new Score("score");
    var swipe = new Swipe(document.getElementById('body'));
    swipe.onDown(function(){
        nuevo.puede_bajar(bloques, filas) ? nuevo.bajar(): ((nuevo = toco_fondo(bloques, filas, cols, score), gameOver(bloques, juego, frame, "game-over", score)));
    });
    swipe.onLeft(function(){
        nuevo.izquierda(bloques);
    });
    swipe.onRight(function(){
        nuevo.derecha(bloques, cols);
    });
    swipe.onUp(function(){
        nuevo.rotar(bloques, filas, cols);
    });
    swipe.run();
    bloques = [];
    nuevo = null;
    siguiente = null;
    document.addEventListener('keydown', function(event) {
        if(nuevo){
            switch(event.keyCode){
                case 37:
                    nuevo.izquierda(bloques);
                    break;
                case 38:
                    nuevo.rotar(bloques, filas, cols);
                    break;
                case 39:
                    nuevo.derecha(bloques, cols);
                    break;
                case 40:
                    nuevo.puede_bajar(bloques, filas) ? nuevo.bajar(): ((nuevo = toco_fondo(bloques, filas, cols, score), gameOver(bloques, juego, frame, "game-over", score)));
                    break;
            }
        }
        console.log(event.keyCode);
    });
    var juego = setInterval(function(){
        if(!nuevo){
            (siguiente == null) ? nuevo = crear_Pieza(cols) : nuevo = siguiente;
            siguiente = crear_Pieza(cols);
            bloques = nuevo.append_bloques(bloques);
        }else{
            nuevo.puede_bajar(bloques, filas) ? nuevo.bajar(): ((nuevo = toco_fondo(bloques, filas, cols, score), gameOver(bloques, juego, frame, "game-over", score)));
        }
    },500);
    
    var frame = setInterval(function(){
        dibujar(bloques,filas,cols,((siguiente != null) ? siguiente.bloquesitos[0].clase : ""));
        score.show();
    },100);
});

//#region Definicion de clases
class Swipe {
    constructor(element) {
        this.xDown = null;
        this.yDown = null;
        this.element = typeof(element) === 'string' ? document.querySelector(element) : element;

        this.element.addEventListener('touchstart', function(evt) {
            this.xDown = evt.touches[0].clientX;
            this.yDown = evt.touches[0].clientY;
        }.bind(this), false);

    }

    onLeft(callback) {
        this.onLeft = callback;

        return this;
    }

    onRight(callback) {
        this.onRight = callback;

        return this;
    }

    onUp(callback) {
        this.onUp = callback;

        return this;
    }

    onDown(callback) {
        this.onDown = callback;

        return this;
    }

    handleTouchMove(evt) {
        if ( ! this.xDown || ! this.yDown ) {
            return;
        }

        var xUp = evt.touches[0].clientX;
        var yUp = evt.touches[0].clientY;

        this.xDiff = this.xDown - xUp;
        this.yDiff = this.yDown - yUp;

        if ( Math.abs( this.xDiff ) > Math.abs( this.yDiff ) ) { // Most significant.
            if ( this.xDiff > 0 ) {
                this.onLeft();
            } else {
                this.onRight();
            }
        } else {
            if ( this.yDiff > 0 ) {
                this.onUp();
            } else {
                this.onDown();
            }
        }

        // Reset values.
        this.xDown = null;
        this.yDown = null;
    }

    run() {
        this.element.addEventListener('touchmove', function(evt) {
            this.handleTouchMove(evt).bind(this);
        }.bind(this), false);
    }
}
class Score{
    constructor(score_counter_id){
        this.score = 0;
        this.score_counter = score_counter_id;
    }
    line(){
        this.score += 100;
    }
    tetris(){
        this.score += 800;
    }
    show(){
        $("#"+this.score_counter).html(this.score);
    }

}
class Block{
    constructor(clase,ubicacion,x,y=0){
        this.clase = "";
        this.x = x;
        this.y = y;
        this.clase = clase;
        this.ubicacion = ubicacion; //posicion en una matriz de 3x3 numerando las posiciones del 0 al 8, de arriba a abajo y de izquierda a derecha.
    }
    bajar(){
        this.y += 1;
    }
    puede_bajar(bloques,filas){
        if(this.y+1 == filas){
            return false;
        }
        let abajo = true;
        bloques.forEach(bloque=>{
            if(bloque.x == this.x && bloque.y == (this.y+1)){
                abajo = false;
            }          
        });
        return abajo;
    }
    derecha(bloques,cols){
        if(this.x +1 == cols){
            return false;
        };
        let derecha = true;
        bloques.forEach(bloque => {
            if(bloque.x == this.x+1 && bloque.y == this.y){
                derecha = false;
            }
        });
        return derecha;
    }
    izquierda(bloques){
        if(this.x -1 < 0){
            return false;
        };
        let izquierda = true;
        bloques.forEach(bloque => {
            if(bloque.x == this.x-1 && bloque.y == this.y){
                izquierda = false;
            }
        });
        return izquierda;
    }
    mover_derecha(){
        this.x += 1;
    }
    mover_izquierda(){
        this.x -= 1;
    }
}
class Pieza{
    constructor(){
        this.bloquesitos = Array(4);
    }
    append_bloques(bloques) {
        for(let w =0; w < 4; w++){
            bloques.push(this.bloquesitos[w]);
        }
        return bloques;  
    }
    bajar(){
        for(let w =0; w < 4; w++){
            this.bloquesitos[w].bajar();
        }
    }
    puede_bajar(bloques, filas){
        let temp = [];
        for(let z = 0; z < 4; z++){
            let abajo = true;
            for(let q = 0; q < 4; q++){
                if(z == q){
                    continue;
                }
                if(this.bloquesitos[z].x == this.bloquesitos[q].x && this.bloquesitos[q].y == (this.bloquesitos[z].y +1)){
                    abajo = false;
                    break;
                }
            }
            if(abajo){
                temp.push(z);
            }
        }
        let puede_bajar = true;
        for(let t = 0; t < temp.length; t++){
            if(!this.bloquesitos[temp[t]].puede_bajar(bloques, filas)){
                puede_bajar = false;
                break;
            }
        }
        return puede_bajar;
    }
    derecha(bloques, cols){
        let temp =[];
        for(let z = 0; z < 4; z++){
            let derecha = true;
            for(let q = 0; q < 4; q++){
                if(z==q){
                    continue;
                }
                if(this.bloquesitos[z].y == this.bloquesitos[q].y && this.bloquesitos[q].x == (this.bloquesitos[z].x+1)){
                    derecha = false;
                    break;
                }
            }
            if(derecha){
                temp.push(z);
            }
        }
        let puede_derecha = true;
        for(let r = 0; r < temp.length; r++){
            if(!this.bloquesitos[temp[r]].derecha(bloques, cols)){
                puede_derecha = false;
                break;
            }
        }
        if(puede_derecha){
            this.bloquesitos.forEach(bloque =>{
                bloque.mover_derecha();
            });
        }
    }
    izquierda(bloques){
        let temp = [];
        for(let z=0; z < 4; z++){
            let izquierda = true;
            for(let q = 0; q < 4; q++){
                if(z==q){
                    continue;
                }
                if(this.bloquesitos[z].y == this.bloquesitos[q].y && this.bloquesitos[q].x == (this.bloquesitos[z].x-1)){
                    izquierda = false;
                    break;
                }
            }
            if(izquierda){
                temp.push(z);
            }
        }
        let puede_izquierda = true;
        for(let l = 0; l < temp.length; l++){
            if(!this.bloquesitos[temp[l]].izquierda(bloques)){
                puede_izquierda = false;
                break;
            }
        }
        if(puede_izquierda){
            this.bloquesitos.forEach(bloque =>{
                bloque.mover_izquierda();
            });
        }
    }
    posicion_disponible(bloques, x, y){
        let disponible = true;
        bloques.forEach(bloque => {
            if(bloque.x == x && bloque.y == y){
               disponible = false;
            }
        });
        return disponible;
    }  
    rotar(bloques, filas, cols){
        let overflow = 0; //0 para ninguno, 1 para arriba, 2 para derecha, 3 para izquierda, 4 para abajo.
        for(let x = 0; x < 4; x++){
            switch(this.bloquesitos[x].ubicacion){
                case 0:
                    if(this.bloquesitos[x].x+2 == cols){
                        overflow = 2;
                    }
                    break;
                case 1:
                    if(this.bloquesitos[x].x+1 == cols){
                        overflow = 2;                       
                    }
                    break;
                case 2:
                    if(this.bloquesitos[x].y+2 == filas){
                        overflow = 4;
                    }
                    break;
                case 3:
                    if(this.bloquesitos[x].y == 0){
                        overflow = 1;
                    }
                    break;
                case 4:
                    break;
                case 5:
                    if(this.bloquesitos[x].y+1 == filas){
                        overflow = 4;
                    }
                    break;
                case 6:
                    if(this.bloquesitos[x].y-2 < 0){
                        overflow = 1;
                    }
                    break;
                case 7:
                    if(this.bloquesitos[x].x-1 < 0){
                        overflow = 3;
                    }
                    break;
                case 8:
                    if(this.bloquesitos[x].x-2 < 0){
                        overflow = 3;
                    }
                    break;
            }
            if(overflow != 0){
                break;
            }
        }
        switch(overflow){
            case 0:
                if(this.puede_rotar(bloques)){
                    this.rotar_aux();
                }
                break;
            case 1: 
                if(this.puede_rotar(bloques,0,1)){
                    this.rotar_aux(0,1);
                }
                break;
            case 2: 
                if(this.puede_rotar(bloques,-1,0)){
                    this.rotar_aux(-1,0);
                }
                break;
            case 3: 
                if(this.puede_rotar(bloques, 1,0)){
                    this.rotar_aux(1,0);
                }
                break;
            case 4:
                if(this.puede_rotar(bloques, 0, -1)){
                    this.rotar_aux(0,-1);
                }
                break;
        }
    }
    rotar_aux(offset_x = 0, offset_y = 0){
        for(let z =0; z <4; z++){
            switch(this.bloquesitos[z].ubicacion){
                case 0:
                    this.bloquesitos[z].x = this.bloquesitos[z].x+2+offset_x;
                    this.bloquesitos[z].y = this.bloquesitos[z].y+offset_y;
                    this.bloquesitos[z].ubicacion = 2;
                    break;
                case 1:
                    this.bloquesitos[z].x = this.bloquesitos[z].x+1+offset_x;
                    this.bloquesitos[z].y = this.bloquesitos[z].y+1+offset_y;
                    this.bloquesitos[z].ubicacion = 5;
                    break;
                case 2:
                    this.bloquesitos[z].x = this.bloquesitos[z].x+offset_x;
                    this.bloquesitos[z].y = this.bloquesitos[z].y+2+offset_y;
                    this.bloquesitos[z].ubicacion = 8;
                    break;
                case 3:
                    this.bloquesitos[z].x = this.bloquesitos[z].x+1+offset_x;
                    this.bloquesitos[z].y = this.bloquesitos[z].y-1+offset_y;
                    this.bloquesitos[z].ubicacion = 1;
                    break;
                case 4:
                    this.bloquesitos[z].x = this.bloquesitos[z].x+offset_x;
                    this.bloquesitos[z].y = this.bloquesitos[z].y+offset_y;
                    break;
                case 5:
                    this.bloquesitos[z].x = this.bloquesitos[z].x-1+offset_x;
                    this.bloquesitos[z].y = this.bloquesitos[z].y+1+offset_y;
                    this.bloquesitos[z].ubicacion = 7;
                    break;
                case 6:
                    this.bloquesitos[z].x = this.bloquesitos[z].x+offset_x;
                    this.bloquesitos[z].y = this.bloquesitos[z].y-2+offset_y;
                    this.bloquesitos[z].ubicacion = 0;
                    break;
                case 7:
                    this.bloquesitos[z].x = this.bloquesitos[z].x-1+offset_x;
                    this.bloquesitos[z].y = this.bloquesitos[z].y-1+offset_y;
                    this.bloquesitos[z].ubicacion = 3;
                    break;
                case 8:
                    this.bloquesitos[z].x = this.bloquesitos[z].x-2+offset_x;
                    this.bloquesitos[z].y = this.bloquesitos[z].y+offset_y;
                    this.bloquesitos[z].ubicacion = 6;
                    break;
            }
        }
    }
    puede_rotar(bloques, offset_x =0, offset_y=0 ){
        let puede_rotar = true;
        for(let x = 0; x < 4; x++){
            let libre = false;
            switch(this.bloquesitos[x].ubicacion){
                case 0:
                    for(let w = 0; w < 4; w++){
                        if(this.bloquesitos[w].x == this.bloquesitos[x].x+2+offset_x && 
                            this.bloquesitos[w].y == this.bloquesitos[x].y+offset_y){
                            libre = true;
                        }
                    }
                    if(libre){
                        break;
                    }
                    if(!this.posicion_disponible(bloques, this.bloquesitos[x].x+2+offset_x, this.bloquesitos[x].y+offset_y)){
                        puede_rotar = false;
                    }
                    break;
                case 1:
                    for(let w = 0; w < 4; w++){
                        if(this.bloquesitos[w].x == this.bloquesitos[x].x+1+offset_x && 
                            this.bloquesitos[w].y == this.bloquesitos[x].y+1+offset_y){
                            libre = true;
                        }
                    }
                    if(libre){
                        break;
                    }
                    if(!this.posicion_disponible(bloques, this.bloquesitos[x].x+1+offset_x, this.bloquesitos[x].y+1+offset_y)){
                        puede_rotar = false;
                    }
                    break;
                case 2:
                    for(let w = 0; w < 4; w++){
                        if(this.bloquesitos[w].x == this.bloquesitos[x].x+offset_x && 
                            this.bloquesitos[w].y == this.bloquesitos[x].y+2+offset_y){
                            libre = true;
                        }
                    }
                    if(libre){
                        break;
                    }
                    if(!this.posicion_disponible(bloques, this.bloquesitos[x].x+offset_x, this.bloquesitos[x].y+2+offset_y)){
                        puede_rotar = false;
                    }
                    break;
                case 3:
                    for(let w = 0; w < 4; w++){
                        if(this.bloquesitos[w].x == this.bloquesitos[x].x+1+offset_x && 
                            this.bloquesitos[w].y == this.bloquesitos[x].y-1+offset_y){
                            libre = true;
                        }
                    }
                    if(libre){
                        break;
                    }
                    if(!this.posicion_disponible(bloques, this.bloquesitos[x].x+1+offset_x, this.bloquesitos[x].y-1+offset_y)){
                        puede_rotar = false;
                    }
                    break;
                case 4:
                    for(let w = 0; w < 4; w++){
                        if(this.bloquesitos[w].x == this.bloquesitos[x].x+offset_x && 
                            this.bloquesitos[w].y == this.bloquesitos[x].y+offset_y){
                            libre = true;
                        }
                    }
                    if(libre){
                        break;
                    }
                    if(!this.posicion_disponible(bloques, this.bloquesitos[x].x+offset_x, this.bloquesitos[x].y+offset_y)){
                        puede_rotar = false;
                    }
                    break;
                case 5:
                    for(let w = 0; w < 4; w++){
                        if(this.bloquesitos[w].x == this.bloquesitos[x].x-1+offset_x && 
                            this.bloquesitos[w].y == this.bloquesitos[x].y+1+offset_y){
                            libre = true;
                        }
                    }
                    if(libre){
                        break;
                    }
                    if(!this.posicion_disponible(bloques, this.bloquesitos[x].x-1+offset_x, this.bloquesitos[x].y+1+offset_y)){
                        puede_rotar = false;
                    }
                    break;
                case 6:
                    for(let w = 0; w < 4; w++){
                        if(this.bloquesitos[w].x == this.bloquesitos[x].x+offset_x && 
                            this.bloquesitos[w].y == this.bloquesitos[x].y-2+offset_y){
                            libre = true;
                        }
                    }
                    if(libre){
                        break;
                    }
                    if(!this.posicion_disponible(bloques, this.bloquesitos[x].x+offset_x, this.bloquesitos[x].y-2+offset_y)){
                        puede_rotar = false;
                    }
                    break;
                case 7:
                    for(let w = 0; w < 4; w++){
                        if(this.bloquesitos[w].x == this.bloquesitos[x].x-1+offset_x && 
                            this.bloquesitos[w].y == this.bloquesitos[x].y-1+offset_y){
                            libre = true;
                        }
                    }
                    if(libre){
                        break;
                    }
                    if(!this.posicion_disponible(bloques, this.bloquesitos[x].x-1+offset_x, this.bloquesitos[x].y-1+offset_y)){
                        puede_rotar = false;
                    }
                    break;
                case 8:
                    for(let w = 0; w < 4; w++){
                        if(this.bloquesitos[w].x == this.bloquesitos[x].x-2+offset_x && 
                            this.bloquesitos[w].y == this.bloquesitos[x].y+offset_y){
                            libre = true;
                        }
                    }
                    if(libre){
                        break;
                    }
                    if(!this.posicion_disponible(bloques, this.bloquesitos[x].x-2+offset_x, this.bloquesitos[x].y+offset_y)){
                        puede_rotar = false;
                    }
                    break;
            }
            if(!puede_rotar){
                break;
            }
        }
        return puede_rotar;
    }
}
class O_pieza extends Pieza{
    constructor(X,cols){
        super();
        let x;
        if(X == cols-1){
            x = X-1;
        }else{
            x = X;
        }
        this.bloquesitos[0]  = new Block("O",0,x);
        this.bloquesitos[1]  = new Block("O",1,x+1);
        this.bloquesitos[2]  = new Block("O",3,x,1);
        this.bloquesitos[3]  = new Block("O",4,x+1,1);
    }
    
    rotar(bloques, filas, cols){
        return null;
    } 
}
class I_pieza extends Pieza{
    constructor(X, cols){
        super();
        let x;
        if(X+4 >= cols){
            x = cols -4;
        }else{
            x = X;
        }
        this.bloquesitos[0] = new Block("I",0,x);
        this.bloquesitos[1] = new Block("I",1,x+1);
        this.bloquesitos[2] = new Block("I",2,x+2);
        this.bloquesitos[3] = new Block("I",3,x+3);
        this.rotacion = 0;
    }
    rotar(bloques, filas, cols){
        if(!this.rotacion){
            if(this.bloquesitos[0].y -2 < 0){
                if(((this.bloquesitos[0].y == 0) ? true : this.posicion_disponible(bloques, this.bloquesitos[1].x,0))&&
                   ((this.bloquesitos[0].y == 1) ? true : this.posicion_disponible(bloques, this.bloquesitos[1].x,1))&&
                   this.posicion_disponible(bloques, this.bloquesitos[1].x,2)&&
                   this.posicion_disponible(bloques, this.bloquesitos[1].x,3)){
                        this.bloquesitos[0].x = this.bloquesitos[1].x;
                        this.bloquesitos[0].y = 0;
                        this.bloquesitos[1].y = 1;
                        this.bloquesitos[2].x = this.bloquesitos[1].x;
                        this.bloquesitos[2].y = 2;
                        this.bloquesitos[3].x = this.bloquesitos[1].x;
                        this.bloquesitos[3].y = 3;
                        this.rotacion = 1;
                        return null;
                }else{
                    return null;
                } 
            }else if(this.bloquesitos[0].y+1 >= filas){
                if(this.posicion_disponible(bloques,this.bloquesitos[1].x,filas-2)&&
                   this.posicion_disponible(bloques,this.bloquesitos[1].x,filas-3)&&
                   this.posicion_disponible(bloques,this.bloquesitos[1].x,filas-4)){
                        this.bloquesitos[0].x = this.bloquesitos[1].x;
                        this.bloquesitos[0].y = filas-4;
                        this.bloquesitos[1].y = filas-3;
                        this.bloquesitos[2].x = this.bloquesitos[1].x;
                        this.bloquesitos[2].y = filas-2;
                        this.bloquesitos[3].x = this.bloquesitos[1].x;
                        this.bloquesitos[3].y = filas-1;
                        this.rotacion = 1;
                        return null;
                }else{
                    return null;
                }
            }else{
                if(this.posicion_disponible(bloques, this.bloquesitos[1].x, this.bloquesitos[1].y-2)&&
                   this.posicion_disponible(bloques, this.bloquesitos[1].x, this.bloquesitos[1].y-1)&&
                   this.posicion_disponible(bloques, this.bloquesitos[1].x, this.bloquesitos[1].y+1)){
                        this.bloquesitos[0].x = this.bloquesitos[1].x;
                        this.bloquesitos[0].y -= 2;
                        this.bloquesitos[1].y -= 1;
                        this.bloquesitos[2].x = this.bloquesitos[1].x;
                        this.bloquesitos[3].x = this.bloquesitos[1].x;
                        this.bloquesitos[3].y += 1;
                        this.rotacion = 1;
                        return null;
                }
            }
        }else{
            if(this.bloquesitos[0].x-1 < 0){
                if(this.posicion_disponible(bloques, 1,this.bloquesitos[2].y)&&
                   this.posicion_disponible(bloques, 2,this.bloquesitos[2].y)&&
                   this.posicion_disponible(bloques, 3,this.bloquesitos[2].y)){
                        this.bloquesitos[0].x = 0;
                        this.bloquesitos[0].y = this.bloquesitos[2].y;
                        this.bloquesitos[1].x = 1;
                        this.bloquesitos[1].y = this.bloquesitos[2].y;
                        this.bloquesitos[2].x = 2;
                        this.bloquesitos[3].x = 3;
                        this.bloquesitos[3].y = this.bloquesitos[2].y;
                        this.rotacion = 0;
                        return null;
                }else{
                    return null;
                }
            }else if(this.bloquesitos[0].x +2 >= cols){
                if(((this.bloquesitos[0].x == cols-1) ? true : this.posicion_disponible(bloques, cols-1, this.bloquesitos[2].y))&&
                   ((this.bloquesitos[0].x == cols-2) ? true : this.posicion_disponible(bloques, cols-2, this.bloquesitos[2].y))&& 
                   this.posicion_disponible(bloques, cols-3, this.bloquesitos[2].y)&&
                   this.posicion_disponible(bloques, cols-4, this.bloquesitos[2].y)){
                        this.bloquesitos[0].x = cols-4;
                        this.bloquesitos[0].y = this.bloquesitos[2].y;
                        this.bloquesitos[1].x = cols-3;
                        this.bloquesitos[1].y = this.bloquesitos[2].y;
                        this.bloquesitos[2].x = cols-2;
                        this.bloquesitos[3].x = cols-1;
                        this.bloquesitos[3].y = this.bloquesitos[2].y;
                        this.rotacion = 0;
                        return null;
                   }else{
                       return null;
                }
            }else{
                if(this.posicion_disponible(bloques, this.bloquesitos[0].x-1, this.bloquesitos[2].y)&&
                   this.posicion_disponible(bloques, this.bloquesitos[0].x+1, this.bloquesitos[2].y)&&
                   this.posicion_disponible(bloques, this.bloquesitos[0].x+2, this.bloquesitos[2].y)){
                        this.bloquesitos[0].x -= 1;
                        this.bloquesitos[0].y = this.bloquesitos[2].y;
                        this.bloquesitos[1].y = this.bloquesitos[2].y;
                        this.bloquesitos[2].x += 1;
                        this.bloquesitos[3].x += 2;
                        this.bloquesitos[3].y = this.bloquesitos[2].y;
                        this.rotacion = 0;
                        return null;
                }
            }
        }

        
    }
}
class S_pieza extends Pieza{
    constructor(X, cols){
        super();
        let x;
        if(X+3 >= cols){
            x = cols -3;
        }else{
            x = X;
        }
        this.bloquesitos[0] = new Block("S",1,x+1);
        this.bloquesitos[1] = new Block("S",2,x+2);
        this.bloquesitos[2] = new Block("S",3,x,1);
        this.bloquesitos[3] = new Block("S",4,x+1,1);
        this.rotacion = 0;
    }
}
class Z_pieza extends Pieza{
    constructor(X, cols){
        super();
        let x;
        if(X+3 >= cols){
            x = cols -3;
        }else{
            x = X;
        }
        this.bloquesitos[0] = new Block("Z",0,x);
        this.bloquesitos[1] = new Block("Z",1,x+1);
        this.bloquesitos[2] = new Block("Z",4,x+1,1);
        this.bloquesitos[3] = new Block("Z",5,x+2,1);
        this.rotacion = 0;
    }
}
class T_pieza extends Pieza{
    constructor(X, cols){
        super();
        let x;
        if(X+3 >= cols){
            x = cols -3;
        }else{
            x = X;
        }
        this.bloquesitos[0] = new Block("T",0,x);
        this.bloquesitos[1] = new Block("T",1,x+1);
        this.bloquesitos[2] = new Block("T",2,x+2);
        this.bloquesitos[3] = new Block("T",4,x+1,1);
        this.rotacion = 0;
    }
}
class L_pieza extends Pieza{
    constructor(X, cols){
        super();
        let x;
        if(X+3 >= cols){
            x = cols -3;
        }else{
            x = X;
        }
        this.bloquesitos[0] = new Block("L",0,x);
        this.bloquesitos[1] = new Block("L",1,x+1);
        this.bloquesitos[2] = new Block("L",2,x+2);
        this.bloquesitos[3] = new Block("L",3,x,1);
        this.rotacion = 0;
    }
}
class J_pieza extends Pieza{
    constructor(X, cols){
        super();
        let x;
        if(X+3 >= cols){
            x = cols -3;
        }else{
            x = X;
        }
        this.bloquesitos[0] = new Block("J",0,x);
        this.bloquesitos[1] = new Block("J",1,x+1);
        this.bloquesitos[2] = new Block("J",2,x+2);
        this.bloquesitos[3] = new Block("J",5,x+2,1);
        this.rotacion = 0;
    }
}
//#endregion

function destruir_si_completa(bloques, num_fila, cols){
    let completa = true;
    for(c = 0; c < cols; c ++){
        let ocupada = false;
        bloques.forEach(bloque=>{
            if(bloque.y == num_fila && bloque.x == c){
                ocupada = true;
            }
        });
        if(!ocupada){
            completa = false;
            break;
        }
    }
    if(completa){
        for(z = bloques.length-1; z >= 0; z--){
            if(bloques[z].y == num_fila){
                bloques.splice(z,1);
            }
        }
        bloques.forEach(bloque =>{
            if(bloque.y < num_fila){
                bloque.y ++;
            }
        });
        return true;
    }
    return false;
}
function dibujar(bloques, filas, columnas, siguiente){
    let tabla = "<table class='tabla'>";
    for(let y=0; y < filas; y++){
        tabla += "<tr>";
        for(let x=0; x < columnas; x++){
            let is_bloque = false;
            bloques.forEach(bloque => {
                if((bloque.x == x && bloque.y == y)){
                    tabla += "<td class='celda bloque "+bloque.clase+"'></td>";
                    is_bloque = true;
                }
            });
            if(is_bloque){
                // tabla += "<td class='bloque'></td>";
            }else{
                tabla += "<td class='celda espacio'></td>";                
            }

        }
        tabla += "</tr>";
    }
    tabla += "</table>";
    $("#tablero").html(tabla);
    $(".ya_no_se_me_ocurren_mas_nombres_para_las_clases").css("padding-left",($(".mitad").outerWidth()-$(".tabla").outerWidth()));
    $(".ya_no_se_me_ocurren_mas_nombres_para_las_clases2").css("padding-left",$(".mitad").outerWidth()-$(".tabla").outerWidth()+$("#texto-score").width()/2)
    let tablita_siguiente = "<table class='tablita-siguiente'>";
    switch(siguiente){
        case "":
            break;
        case "I":
            tablita_siguiente += "<tr><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td></tr>";
            tablita_siguiente += "<tr><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td></tr>";
            tablita_siguiente += "<tr><td class='celda bloque I'></td><td class='celda bloque I'></td><td class='celda bloque I'></td><td class='celda bloque I'></td></tr>";
            tablita_siguiente += "<tr><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td></tr>";
            break;
        case "S":
            tablita_siguiente += "<tr><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td></tr>";
            tablita_siguiente += "<tr><td class='celda espacio nadita'></td><td class='celda bloque S'></td><td class='celda bloque S'></td></tr>";
            tablita_siguiente += "<tr><td class='celda bloque S'></td><td class='celda bloque S'></td><td class='celda espacio nadita'></td></tr>";
            break;
        case "Z":
            tablita_siguiente += "<tr><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td></tr>";
            tablita_siguiente += "<tr><td class='celda bloque Z'></td><td class='celda bloque Z'></td><td class='celda espacio nadita'></td></tr>";
            tablita_siguiente += "<tr><td class='celda espacio nadita'></td><td class='celda bloque Z'></td><td class='celda bloque Z'></td></tr>";
            break;
        case "T":
            tablita_siguiente += "<tr><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td></tr>";
            tablita_siguiente += "<tr><td class='celda bloque T'></td><td class='celda bloque T'></td><td class='celda bloque T'></td></tr>";
            tablita_siguiente += "<tr><td class='celda espacio nadita'></td><td class='celda bloque T'></td><td class='celda espacio nadita'></td></tr>";
            break;
        case "O":
            tablita_siguiente += "<tr><td class='celda bloque O'></td><td class='celda bloque O'></td></tr>"
            tablita_siguiente += "<tr><td class='celda bloque O'></td><td class='celda bloque O'></td></tr>"
            break;
        case "L":
            tablita_siguiente += "<tr><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td></tr>";
            tablita_siguiente += "<tr><td class='celda bloque L'></td><td class='celda bloque L'></td><td class='celda bloque L'></td></tr>";
            tablita_siguiente += "<tr><td class='celda bloque L'></td><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td></tr>";
            break;
        case "J":
            tablita_siguiente += "<tr><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td></tr>";
            tablita_siguiente += "<tr><td class='celda bloque J'></td><td class='celda bloque J'></td><td class='celda bloque J'></td></tr>";
            tablita_siguiente += "<tr></td><td class='celda espacio nadita'></td><td class='celda espacio nadita'></td><td class='celda bloque J'></tr>";
            break;
    }
    tablita_siguiente += "</table>";
    $("#siguiente").html(tablita_siguiente);
    $("#siguiente").css("padding-left",($("#sig_pieza").outerWidth()-$(".tablita-siguiente").width())/2);
}
function toco_fondo(bloques, filas, cols, score){
    let cont = 0;
    for(let z =0;z < filas; z++){
        destruir_si_completa(bloques,z,cols) ? cont ++ : null;
    }
    switch(cont){
        case 0:
            break;
        case 4:
            score.tetris();
            break;
        default:
            score.line();
            break;
    }
    return null;
}
function crear_Pieza(cols){
    switch(randomInteger(0, 6)){
        case 0:
            return new O_pieza(randomInteger(0,cols-2), cols);
        case 1: 
            return new I_pieza(randomInteger(0, cols-4), cols);
        case 2: 
            return new S_pieza(randomInteger(0, cols-3), cols);
        case 3:
            return new Z_pieza(randomInteger(0, cols-3), cols);
        case 4:
            return new T_pieza(randomInteger(0, cols-3), cols);
        case 5:
            return new L_pieza(randomInteger(0, cols-3), cols);
        case 6:
            return new J_pieza(randomInteger(0, cols-3), cols);
    }
}
function randomInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function gameOver(bloques, juego, dibujo, div_over, score){
    let over = false;
    bloques.forEach(bloque => {
        if(bloque.y == 0){
            over = true;
        }
    });
    if(over){
        clearInterval(juego);
        clearInterval(dibujo);
        let msg = "<h1 class='game-over'>Game Over</h1><h2 class='puntaje-final'>Tu puntaje fue de <span class='puntaje-final-num'>"+score.score+"</span> puntos.</h2>";
        $("#"+div_over).html(msg);
    }
}
